﻿using UnityEngine;
using System.Collections;

public class Tile : MonoBehaviour
{
    private GameManager m_manager;
    public GameManager.Symbols Symbol { get; private set; }
    private Transform m_child;
    private Collider m_collider;

    //Getting the first child and grabbing the collider
    private void Start()
    {
        m_child = this.transform.GetChild( 0 );
        m_collider = GetComponent<Collider>();
    }

    //When the mouse is pressed on the tile, it is selected
    private void OnMouseDown()
    {
        m_manager.TileSelected( this );
    }

    //Creates the symbols on tiles and calls the manager script
    public void Initialize( GameManager manager, GameManager.Symbols symbol )
    {
        m_manager = manager;
        Symbol = symbol;
    }

    //Stops coroutines, it will do a spin to reveal itself and turn off the collider
    public void Reveal()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 180, 0.8f ) );
        m_collider.enabled = false;
    }

    //Stops coroutines, it will do a spin to hides itself and turn on the collider
    public void Hide()
    {
        StopAllCoroutines();
        StartCoroutine( Spin( 0, 0.8f ) );
        m_collider.enabled = true;
    }

    //Continously rotating towards target angle until timer reaches the time
    private IEnumerator Spin( float target, float time )
    {
        float timer = 0;
        float startingRotation = m_child.eulerAngles.y;
        Vector3 euler = m_child.eulerAngles;
        while ( timer < time )
        {
            timer += Time.deltaTime;
            euler.y = Mathf.LerpAngle( startingRotation, target, timer / time );
            m_child.eulerAngles = euler;
            yield return null;
        }
        euler.y = target;
        m_child.eulerAngles = euler;
    }
}
