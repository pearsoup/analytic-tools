﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;


public class GameManager : MonoBehaviour
{
    public static float totalPlayTime;
    public static float timeBetweenMatching;
    public static List<float> averageTimeBetweenMatching = new List<float>();
    public static List<float> averageLevelTimes = new List<float>();
    public static int CurrentLevelNumber;
    public static int failedMatches;

    [System.Flags]
    public enum Symbols
    {
        Waves = 1 << 0,
        Dot = 1 << 1,
        Square = 1 << 2,
        LargeDiamond = 1 << 3,
        SmallDiamonds = 1 << 4,
        Command = 1 << 5,
        Bomb = 1 << 6,
        Sun = 1 << 7,
        Bones = 1 << 8,
        Drop = 1 << 9,
        Face = 1 << 10,
        Hand = 1 << 11,
        Flag = 1 << 12,
        Disk = 1 << 13,
        Candle = 1 << 14,
        Wheel = 1 << 15
    }

    [System.Serializable]
    public struct LevelDescription
    {
        public int Rows, Columns;
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;

    private void Start()
    {
        LoadLevel(CurrentLevelNumber);
    }

    private void Update()
    {
        totalPlayTime += Time.deltaTime;
        if (m_tileOne != null)
        {
            timeBetweenMatching += Time.deltaTime;
        }
    }

    //Creates a level based off the information you have stated in that particular Level Elements
    private void LoadLevel(int levelNumber)
    {
        LevelDescription level = Levels[levelNumber % Levels.Length];

        List<Symbols> symbols = GetRequiredSymbols(level);

        //Grabs the information from the Levels Element and instantiates rows and columns depending on the size you have stated
        //Also randomly assigns the symbols that you have chosen and changes it in the renderer
        for (int rowIndex = 0; rowIndex < level.Rows; ++rowIndex)
        {
            float yPosition = rowIndex * (1 + TileSpacing);
            for (int colIndex = 0; colIndex < level.Columns; ++colIndex)
            {
                float xPosition = colIndex * (1 + TileSpacing);
                GameObject tileObject = Instantiate(TilePrefab, new Vector3(xPosition, yPosition, 0), Quaternion.identity, this.transform);
                int symbolIndex = UnityEngine.Random.Range(0, symbols.Count);
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol(symbols[symbolIndex]);
                tileObject.GetComponent<Tile>().Initialize(this, symbols[symbolIndex]);
                symbols.RemoveAt(symbolIndex);
            }
        }

        SetupCamera(level);
    }

    //Converts the chosen symbols, translates them, and assigns the symbols to the cards on the level while making checking for arguements
    private List<Symbols> GetRequiredSymbols(LevelDescription level)
    {
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //Checks which symbol you have chosen for that specific level and adds the symbols while making sure that there are an even number of each card symbol to fill in that level
        {
            Array allSymbols = Enum.GetValues(typeof(Symbols));

            m_cardsRemaining = cardTotal;

            if (cardTotal % 2 > 0)
            {
                new ArgumentException("There must be an even number of cards");
            }

            foreach (Symbols symbol in allSymbols)
            {
                if ((level.UsedSymbols & symbol) > 0)
                {
                    symbols.Add(symbol);
                }
            }
        }

        //If no symbols have been assigned to the level or if there are too many symbols for the total amount of cards, an arguement is created
        {
            if (symbols.Count == 0)
            {
                new ArgumentException("The level has no symbols set");
            }
            if (symbols.Count > cardTotal / 2)
            {
                new ArgumentException("There are too many symbols for the number of cards.");
            }
        }

        //Makes its so each symbol chosen would have two tiles
        {
            int repeatCount = (cardTotal / 2) - symbols.Count;
            if (repeatCount > 0)
            {
                List<Symbols> symbolsCopy = new List<Symbols>(symbols);
                List<Symbols> duplicateSymbols = new List<Symbols>();
                for (int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex)
                {
                    int randomIndex = UnityEngine.Random.Range(0, symbolsCopy.Count);
                    duplicateSymbols.Add(symbolsCopy[randomIndex]);
                    symbolsCopy.RemoveAt(randomIndex);
                    if (symbolsCopy.Count == 0)
                    {
                        symbolsCopy.AddRange(symbols);
                    }
                }
                symbols.AddRange(duplicateSymbols);
            }
        }

        symbols.AddRange(symbols);

        return symbols;
    }

    //finding the offset of the Cards sheet to find the symbol it needs
    private Vector2 GetOffsetFromSymbol(Symbols symbol)
    {
        Array symbols = Enum.GetValues(typeof(Symbols));
        for (int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex)
        {
            if ((Symbols)symbols.GetValue(symbolIndex) == symbol)
            {
                return new Vector2(symbolIndex % 4, symbolIndex / 4) / 4f;
            }
        }
        Debug.Log("Failed to find symbol");
        return Vector2.zero;
    }

    //adjusts the camera to fit all the cards on screen according to the row and column size
    private void SetupCamera(LevelDescription level)
    {
        Camera.main.orthographicSize = (level.Rows + (level.Rows + 1) * TileSpacing) / 2;
        Camera.main.transform.position = new Vector3((level.Columns * (1 + TileSpacing)) / 2, (level.Rows * (1 + TileSpacing)) / 2, -10);
    }

    //When the first tile is selected, it will reveal itself, when the second tile is selected, it will wait for 1 second and then proceed to its next step
    //depending on whether it was matching with the first tile or not, it will be set between true or false
    public void TileSelected(Tile tile)
    {
        if (m_tileOne == null)
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if (m_tileTwo == null)
        {
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if (m_tileOne.Symbol == m_tileTwo.Symbol)
            {
                averageTimeBetweenMatching.Add(timeBetweenMatching);
                timeBetweenMatching = 0;
                StartCoroutine(WaitForHide(true, 1f));
            }
            else
            {
                averageTimeBetweenMatching.Add(timeBetweenMatching);
                timeBetweenMatching = 0;
                failedMatches++ ;
                StartCoroutine(WaitForHide(false, 1f));
            }
        }
    }

    //If the level is completed and is the final level, game is over, else it will load the next level
    private void LevelComplete()
    {
        ++CurrentLevelNumber;

        averageLevelTimes.Add(Time.timeSinceLevelLoad);

        float totalAverageTimeBetweenMatching = 0;

        foreach(float matchingTime in averageTimeBetweenMatching)
        {
            totalAverageTimeBetweenMatching += matchingTime; 
        }
        totalAverageTimeBetweenMatching /= averageTimeBetweenMatching.Count;

        averageTimeBetweenMatching = new List<float>();

        Analytics.CustomEvent("averageTimeBetweenMatching", new Dictionary<string, object>
        {
            { "timeBetweenMatching", totalAverageTimeBetweenMatching },
        });


        if (CurrentLevelNumber > Levels.Length - 1)
        {
            Debug.Log("GameOver");
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    private void OnApplicationQuit()
    {
        float totalAverageLevelTimes = 0;

        foreach(float levelTime in averageLevelTimes)
        {
            totalAverageLevelTimes += levelTime; 
        }
        totalAverageLevelTimes /= averageLevelTimes.Count;

        Analytics.CustomEvent("averageTimeLevelCompleted", new Dictionary<string, object>
        {
            { "averageLevelTime", totalAverageLevelTimes },
        });

        Analytics.CustomEvent("completedLevels", new Dictionary<string, object>
        {
            { "levelsCompleted", CurrentLevelNumber },
        });

        Analytics.CustomEvent("timeTotal", new Dictionary<string, object>
        {
            { "finishedPlaying", totalPlayTime },
        });

        Analytics.CustomEvent("totalFailedMatchesInGame", new Dictionary<string, object>
        {
            { "totalFailedMatches", failedMatches },
        });
    }

    //when the timer for the second revealed card flipped is 0, if the first and second card are matching, destroy the selected gameobjects and subtract 2 from the remaining cards
    //if they don't match however, hide it again
    //once no cards remain, the level is complete and go LevelComplete function
    private IEnumerator WaitForHide(bool match, float time)
    {
        float timer = 0;
        while (timer < time)
        {
            timer += Time.deltaTime;
            if (timer >= time)
            {
                break;
            }
            yield return null;
        }
        if (match)
        {
            Destroy(m_tileOne.gameObject);
            Destroy(m_tileTwo.gameObject);
            m_cardsRemaining -= 2;
        }
        else
        {
            m_tileOne.Hide();
            m_tileTwo.Hide();
        }
        m_tileOne = null;
        m_tileTwo = null;

        if (m_cardsRemaining == 0)
        {
            LevelComplete();
        }
    }
}

